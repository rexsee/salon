<?php

namespace App\Http\Controllers\Staff;

use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ServiceController extends Controller
{
    public function index()
    {
        $result = Service::all();
        return view('staff.service.index', compact('result'));
    }

    public function add(Request $request)
    {
        if ($request->method() == 'POST') {
            $inputs = $request->validate([
                'name' => 'required|max:191',
                'type' => 'required|max:191',
                'price' => 'required',
                'order' => 'integer',
                'price_type' => 'required|in:Estimate,Net',
//                'minutes_needed' => 'required|numeric',
                'description' => 'max:60',
                'image' => 'nullable|mimes:jpeg,jpg,png,gif|max:5120',
            ]);

            if (!empty($request->image)) {
                $image = $request->file('image');
                $file_name = 'service_' . time();
                $full_file_name = $file_name . '.' . $image->getClientOriginalExtension();
                $image->move('storage/service', $full_file_name);
                //Image::make('storage/service/' . $full_file_name)->fit('500', '700')->save();
                //Image::make('storage/service/' . $full_file_name)->save('storage/service/' . $file_name . '@2x.' . $image->getClientOriginalExtension());
                $inputs['image_path'] = 'storage/service/' . $full_file_name;
            }

            Service::create($inputs);

            flash('Record added')->success();
            return redirect()->route('staff.service');
        } else {
            return view('staff.service.add');
        }

    }

    public function edit($id, Request $request)
    {
        $record = Service::findOrFail($id);
        if ($request->method() == 'POST') {
            $inputs = $request->validate([
                'name' => 'required|max:191',
                'type' => 'required|max:191',
                'price' => 'required',
                'order' => 'integer',
                'price_type' => 'required|in:Estimate,Net',
//                'minutes_needed' => 'required|numeric',
                'description' => 'max:60',
                'image' => 'nullable|mimes:jpeg,jpg,png,gif|max:5120',
            ]);

            if (!empty($request->image)) {
                if (File::exists($record->image_path)) {
                    File::delete($record->image_path);
                }

                $image = $request->file('image');
                $file_name = 'service_' . time();
                $full_file_name = $file_name . '.' . $image->getClientOriginalExtension();
                $image->move('storage/service', $full_file_name);
                //Image::make('storage/service/' . $full_file_name)->fit('500', '700')->save();
                //Image::make('storage/service/' . $full_file_name)->save('storage/service/' . $file_name . '@2x.' . $image->getClientOriginalExtension());
                $inputs['image_path'] = 'storage/service/' . $full_file_name;
            }

            $record->update($inputs);

            flash('Record updated')->success();
            return redirect()->route('staff.service');
        } else {

            return view('staff.service.edit', compact('record'));
        }
    }

    public function delete($id)
    {
        $record = Service::findOrFail($id);

        if (File::exists($record->image_path)) {
            File::delete($record->image_path);
        }

        $record->delete();

        flash('Record deleted')->warning()->important();
        return redirect()->back();
    }
}
