$(document).ready(function () {
    "use strict";

    // InView
    var $fadeInDown = $('.menu, .menu-mobile, .header h1, .header .subtitle, .topics h3, .topics div i, .speakers .single h3, .membership, .my-event-item, .event-item, #selectCountryForm, .bullets2 .itembox, .about-core-row');
    var $fadeInLeft = $('.h3');
    var $fadeInRight = $('.register-now, .speakers .subtitle, .schedule .subtitle, .registration .subtitle, .registration .price, .sponsors .subtitle, .location .subtitle, .location .address, .social .subtitle');

    $fadeInDown.css('opacity', 0);
    $fadeInLeft.css('opacity', 0);
    $fadeInRight.css('opacity', 0);

    // InView - fadeInDown
    $fadeInDown.one('inview', function (event, visible) {
        if (visible) {
            $(this).addClass('animated fadeInDown');
        }
    });

    // InView - fadeInLeft
    $fadeInLeft.one('inview', function (event, visible) {
        if (visible) {
            $(this).addClass('animated fadeInLeft');
        }
    });

    // InView - fadeInRight
    $fadeInRight.one('inview', function (event, visible) {
        if (visible) {
            $(this).addClass('animated fadeInRight');
        }
    });

});
(function ($) {
    // the sameHeight functions makes all the selected elements of the same height
    $.fn.sameHeight = function () {
        var selector = this;
        var heights = [];

        // Save the heights of every element into an array
        selector.each(function () {
            var height = $(this).height();
            heights.push(height);
        });

        // Get the biggest height
        var maxHeight = Math.max.apply(null, heights);

        // Set the maxHeight to every selected element
        selector.each(function () {
            $(this).height(maxHeight);
        });
    };

}(jQuery));