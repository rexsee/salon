<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>{{ config('app.name', 'Laravel') }} @yield('title')</title>

    <meta name="description" content="Skylith - Viral & Creative Multipurpose HTML template.">
    <meta name="keywords" content="portfolio, clean, minimal, blog, template, portfolio website">
    <meta name="author" content="nK">

    <link rel="icon" type="image/png" href="{{asset('images/favicon.ico')}}">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- START: Styles -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i%7cWork+Sans:400,500,700%7cPT+Serif:400i,500i,700i" rel="stylesheet">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('vendor/bootstrap/dist/css/bootstrap.min.css')}}">

    <!-- FontAwesome -->
    <script defer src="{{asset('vendor/fontawesome-free/js/all.js')}}"></script>
    <script defer src="{{asset('vendor/fontawesome-free/js/v4-shims.js')}}"></script>

    <!-- Stroke 7 -->
    <link rel="stylesheet" href="{{asset('vendor/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css')}}">

    <!-- Flickity -->
    <link rel="stylesheet" href="{{asset('vendor/flickity/dist/flickity.min.css')}}">

    <!-- Photoswipe -->
    <link rel="stylesheet" href="{{asset('vendor/photoswipe/dist/photoswipe.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/photoswipe/dist/default-skin/default-skin.css')}}">

    <!-- JustifiedGallery -->
    <link rel="stylesheet" href="{{asset('vendor/justifiedGallery/dist/css/justifiedGallery.min.css')}}">


    <!-- jQuery -->
    <script src="{{asset('vendor/jquery/dist/jquery.min.js')}}"></script>

    <link href="{{ mix('css/frontv2.css') }}" rel="stylesheet">
    @yield('style')

    @if(env('APP_ENV') == 'production')
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131352123-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-131352123-1');
        </script>
    @endif
</head>

<body>

<header class="nk-header {{!empty($isFullNav) ? '' : 'nk-header-opaque'}}">
    <nav class="{{!empty($isFullNav) ? 'nk-navbar-transparent nk-navbar-transparent-always' : ''}} nk-navbar nk-navbar-top nk-navbar-dark">
        <div class="{{!empty($isFullNav) ? 'container-fluid' : 'container'}}">
            <div class="nk-nav-table">

                <a href="{{route('index')}}" class="nk-nav-logo">
                    <img src="{{asset('images/logo.png')}}" alt="" width="70" class="nk-nav-logo-img-dark">
                    <img src="{{asset('images/logo-intro.png')}}" alt="" width="70" class="nk-nav-logo-img-light">
                </a>

            </div>
        </div>
    </nav>

</header>

@yield('content')

<footer class="nk-footer" style="background-color: #0b0b0b;">


    <div class="nk-footer-cont nk-footer-cont-md">
        <div class="container text-center">
            <div class="row vertical-gap sm-gap align-items-center">

                <div class="col-lg-5 order-lg-3 text-lg-right">
                    <div class="nk-social text-gray-1">
                        <ul>
                            <li><a class="nk-social-facebook" target="_blank" href="https://www.facebook.com/Alphstudioplus/"><span><span class="nk-social-front fa fa-facebook"></span><span class="nk-social-back fa fa-facebook"></span></span></a></li>
                            <li><a class="nk-social-instagram" target="_blank" href="https://www.instagram.com/alphstudio/"><span><span class="nk-social-front fa fa-instagram"></span><span class="nk-social-back fa fa-instagram"></span></span></a></li>
                        </ul>
                    </div>
                </div>



                <div class="col-lg-5 text-lg-left">
                    <div class="nk-footer-text text-gray">
                        <p>{{date('Y')}} &copy; {{env('APP_NAME')}} - All rights reserved</p>
                    </div>
                </div>

                <div class="col-lg-2">

                    <a class="nk-footer-scroll-top-btn-2 nk-anchor  text-gray-1" href="#top">
                        Go To Top
                    </a>

                </div>
            </div>
        </div>
    </div>
</footer>



<!-- START: Scripts -->

<!-- Object Fit Polyfill -->
<script src="{{asset('vendor/object-fit-images/dist/ofi.min.js')}}"></script>

<!-- ImagesLoaded -->
<script src="{{asset('vendor/imagesloaded/imagesloaded.pkgd.min.js')}}"></script>

<!-- GSAP -->
<script src="{{asset('vendor/gsap/dist/gsap.min.js')}}"></script>
<script src="{{asset('vendor/gsap/dist/ScrollToPlugin.min.js')}}"></script>

<!-- Popper -->
<script src="{{asset('vendor/popper.js/dist/umd/popper.min.js')}}"></script>

<!-- Bootstrap -->
<script src="{{asset('vendor/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<!-- Sticky Kit -->
<script src="{{asset('vendor/sticky-kit/dist/sticky-kit.min.js')}}"></script>

<!-- Jarallax -->
<script src="{{asset('vendor/jarallax/dist/jarallax.min.js')}}"></script>
{{--<script src="assets/vendor/jarallax/dist/jarallax-video.min.js"></script>--}}

<!-- Flickity -->
<script src="{{asset('vendor/flickity/dist/flickity.pkgd.min.js')}}"></script>

<!-- Isotope -->
<script src="{{asset('vendor/isotope-layout/dist/isotope.pkgd.min.js')}}"></script>

<!-- Photoswipe -->
<script src="{{asset('vendor/photoswipe/dist/photoswipe.min.js')}}"></script>
<script src="{{asset('vendor/photoswipe/dist/photoswipe-ui-default.min.js')}}"></script>

<!-- JustifiedGallery -->
<script src="{{asset('vendor/justifiedGallery/dist/js/jquery.justifiedGallery.min.js')}}"></script>

<!-- Hammer.js -->
<script src="{{asset('vendor/hammerjs/hammer.min.js')}}"></script>

<!-- Keymaster -->
<script src="{{asset('vendor/keymaster/keymaster.js')}}"></script>

<script src="{{ asset('js/frontv2.js') }}"></script>
<script>
    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-CSRF-TOKEN", "{{csrf_token()}}");
            xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        },
        method: 'POST',
        dataType: "json",
        cache: false,
    });
</script>
@yield('js')

</body>
</html>