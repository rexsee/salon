<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>{{ config('app.name', 'Laravel') }} @yield('title')</title>

    <meta name="description" content="@yield('description')">
    <meta name="author" content="Rex">

    <link rel="icon" type="image/png" href="{{asset('images/favicon.ico')}}">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- START: Styles -->

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display&display=swap" rel="stylesheet">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('vendor/bootstrap-5.3.0/css/bootstrap.min.css')}}">
    <!-- Stroke 7 -->
    <link rel="stylesheet" href="{{asset('vendor/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css')}}">
    <!-- Flickity -->
    <link rel="stylesheet" href="{{asset('vendor/flickity/dist/flickity.min.css')}}">
    <!-- Photoswipe -->
    <link rel="stylesheet" href="{{asset('vendor/photoswipe/photoswipe.css')}}">
    <!-- jQuery -->
    <script src="{{asset('vendor/jquery/dist/jquery.min.js')}}"></script>

    <link href="{{ mix('css/frontv3.css') }}" rel="stylesheet">
    @yield('style')

    @if(env('APP_ENV') == 'production')
        <!-- Google tag (gtag.js) -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-5242H9ZNC4"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'G-5242H9ZNC4');
        </script>

        <!--
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131352123-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());

            gtag('config', 'UA-131352123-1');
        </script>
        -->
    @endif
</head>

<body>

@yield('content')

<!-- START: Scripts -->
<!-- ImagesLoaded -->
<script src="{{asset('vendor/imagesloaded/imagesloaded.pkgd.min.js')}}"></script>
<!-- GSAP -->
<script src="{{asset('vendor/gsap/dist/gsap.min.js')}}"></script>
<script src="{{asset('vendor/gsap/dist/ScrollToPlugin.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('vendor/bootstrap-5.3.0/js/bootstrap.min.js')}}"></script>
<!-- Flickity -->
<script src="{{asset('vendor/flickity/dist/flickity.pkgd.min.js')}}"></script>
<!-- Photoswipe -->
<script src="{{asset('vendor/photoswipe/photoswipe.umd.min.js')}}"></script>
<script src="{{asset('vendor/photoswipe/photoswipe-lightbox.umd.min.js')}}"></script>
<!-- Inview -->
<script src="{{asset('vendor/inview/in-view.min.js')}}"></script>

<script src="{{ asset('js/frontv3.js') }}"></script>
<script>
    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-CSRF-TOKEN", "{{csrf_token()}}");
            xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        },
        method: 'POST',
        dataType: "json",
        cache: false,
    });
</script>
@yield('js')

</body>
</html>