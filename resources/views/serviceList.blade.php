@extends('layouts.frontv2')
@section('title') - Services @stop

@section('content')

    <div class="nk-main nk-main-dark">

        <div class="container">
            <div class="nk-blog-isotope-4 nk-isotope nk-isotope-3-cols">

                @foreach($data as $item)
                    <div class="nk-isotope-item">
                        <div class="nk-blog-post">
                            <div class="nk-post-thumb">
                                <div class="bg-image">
                                    <img src="{{asset($item['image_path'] ?? 'images/services/default.jpg')}}" alt="">
                                    <div class="bg-image-overlay" style="background-color: rgba(0,0,0,0.5);"></div>
                                </div>
                            </div>

                            <div class="nk-post-content">
                                <div class="nk-post-date">{{$item['description']}}</div>

                                <h2 class="nk-post-title fw-400">{{$item['name']}}</h2>

                                <span>RM {{number_format($item['price'],2)}} {{$item['price_type'] == 'Net' ? '' : '++'}}</span>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
        <div class="nk-gap-5"></div>

    </div>
@stop