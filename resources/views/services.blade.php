@extends('layouts.frontv2',['isFullNav'=>1])
@section('title') - Services @stop

@section('content')
    <div class="nk-main nk-main-dark">

        <div class="nk-gap-6"></div>

        <div class="nk-carousel-2 nk-carousel-x05 nk-carousel-all-visible nk-carousel-dots-3 nk-carousel-arrows-2 nk-carousel-arrows-bottom-center" data-autoplay="18000" data-loop="false" data-dots="true" data-arrows="true" data-cell-align="center" data-parallax="false">
            <div class="nk-carousel-inner">
                @foreach($data as $code => $item)
                    <div>
                        <div>
                            <div class="nk-carousel-absolute-item nk-carousel-active-show">
                                <div class="row align-items-center">
                                    <div class="col-lg-8">
                                        <h3 class="display-3">{{$item['name']}}</h3>
                                    </div>
                                </div>
                            </div>
                            <a href="{{route('services',['category'=>$code])}}" class="nk-carousel-absolute-item"></a>
                            <div class="row">
                                <div class="col-lg-10 offset-lg-1">
                                    <img src="{{asset($item['src'])}}" alt="" class="nk-img-stretch op-8">
                                </div>
                            </div>
                        </div>
                    </div>

                @endforeach
            </div>
        </div>

        <div class="nk-gap-6"></div>

    </div>
@stop