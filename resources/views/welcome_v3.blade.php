@extends('layouts.frontv3')
@section('description')
    {{$system_info->slogan}}
@stop
@section('style')
    <style>

    </style>
@stop
@section('content')
    <div style="height: 2vh"></div>
    <nav class="navbar bg-white navbar-expand-lg sticky-top" data-bs-theme="white">
        <div class="container-fluid">
            <a class="navbar-brand href="{{route('index')}}">
                <img src="{{asset('images/logo-black-2023.png')}}" alt="{{env('APP_NAME')}}">
            </a>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggler"
                    aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarToggler">
                <ul class="navbar-nav w-100 d-flex justify-content-evenly">
                    <li class="nav-item"><a class="nav-link fs-6 text-dark" href="#about">ABOUT US</a></li>
                    <li class="nav-item"><a class="nav-link fs-6 text-dark" href="#team">TEAM</a></li>
                    <li class="nav-item"><a class="nav-link fs-6 text-dark" href="#services">SERVICES</a></li>
                    <li class="nav-item"><a class="nav-link fs-6 text-dark" href="#products">PRODUCTS</a></li>
                    <li class="nav-item"><a class="nav-link fs-6 text-dark" href="#artwork">ARTWORK</a></li>
                    <li class="nav-item"><a class="nav-link fs-6 text-dark" href="#news">NEWS</a></li>
                    <li class="nav-item"><a class="nav-link fs-6 text-dark" href="#contact">CONTACT</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <table class="w-100">
        <tr>
            <td class="text-center text-rotate-vertical feature-td"
                style="background: url('{{asset('images/feature_line.png')}}') 50% 0 no-repeat;">
                <span>&nbsp;</span>
                <span class="fs-4 feature-td-text">{{env('APP_NAME')}}</span>
            </td>

            <td class="text-white coverimg-td" style="background: url('{{asset($system_info->image_path)}}') 50% 0 no-repeat;background-size: cover;">
                <h2 class="text-style-feature-alph">ALPH<br/>STUDIO</h2>
                <span class="text-style-feature-slogan">{{$system_info->slogan}}</span>
            </td>
        </tr>
    </table>

    <div class="container clearfix">
        <div class="section-margin-top">
            <div class="sameHeightVision master-vision-contents">
                <h3 class="fs-2 master-vision-title">THE<br/>MASTER VISION</h3>
                <p class="bg-white mb-0 master-vision-desc">{!! nl2br($system_info->vision_desc) !!}</p>
            </div>
            <div class="master-vision-imgs" id="masterVisionFlickity">
                @foreach($vision_images as $image)
                    <img src="{{asset($image->image_path)}}" class="pull-right sameHeightVision">
                @endforeach
            </div>
            <div class="master-vision-desc-mobile">
                <h3 class="fs-2 master-vision-title">THE<br/>MASTER VISION</h3>
                <p>{!! nl2br($system_info->vision_desc) !!}</p>
            </div>
        </div>
    </div>

    <div class="clearfix scroll-margin section-margin-top team-section-margin-top" id="team">
        @if(!empty($team))
            <h3 class="text-center">Our TEAM</h3>

            <div id="teamFlickity">
                @foreach($team as $item)
                    <div class="carousel-team-cell">
                        <img src="{{asset($item->avatar_path)}}" alt="{{$item->name}}" width="100%">
                        <h4 class="p-2 pt-4 pb-0">{{ucwords(strtolower($item->name))}}</h4>
                        <span class="p-2 pt-0">{{$item->title}}</span>
                    </div>
                @endforeach
            </div>
        @endif
    </div>

    <div class="clearfix section-margin-top scroll-margin" id="about">
        <div class="w-100 text-center about-bg" style="background: url({{asset($system_info->hover_image_path)}}) 50% 0 no-repeat fixed;"></div>
        <div class="container" id="about-wrapper">
            <div class="row about-box">
                <div class="col-12 text-center">
                    <span class="text-white text-style-feature text-feature-about-us">About Us</span>
                </div>
                <div class="col-12 col-sm-3 text-center mobile-hide">
                    <img src="{{asset('images/logo-black-2023.png')}}" class="img-logo-about pb-4" alt="{{env('APP_NAME')}}"/>
                </div>
                <div class="col-12 col-sm-9">
                    <p class="mb-0" style="text-align: justify">{!! nl2br($system_info->about_us_desc) !!}</p>
                </div>
            </div>
        </div>
    </div>

    @if(!empty($slider_images))
        <div class="clearfix section-margin-top">
            <div class="container">
                <h3 class="text-center">Our GALLERY</h3>
                <div class="row popup-gallery">
                    @foreach($slider_images as $id=>$image)
                        <a href="{{asset($image->image_path)}}" data-pswp-width="100" data-pswp-height="600" data-pswp-srcset class="{{$id == 0 ? 'col-12' : 'col-6 col-sm-3'}} p-1">
                            <img src="{{asset($image->image_path)}}" width="100%">
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    @if(!empty($artwork))
        <div class="clearfix scroll-margin" id="artwork">
            <div style="margin-top: 15vh;margin-bottom: 10px;">
                <h3 class="text-center">ALPH ARTWORK</h3>

                <div class="popup-gallery" id="artworkFlickity">
                    @foreach($artwork->take(18) as $item)
                        <a href="{{asset($item->image_path)}}" data-pswp-width="800" data-pswp-height="1000" data-pswp-srcset class="carousel-artwork-cell">
                            <img src="{{asset($item->image_path)}}" alt="{{$item->title}}" width="100%">
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    @if(!empty($serviceV2))
        <div class="section-margin-top scroll-margin" id="services">
            <div class="container">
                <h3 class="text-center">ALPH SERVICES</h3>

                <ul class="nav nav-tabs justify-content-center" style="border: unset" id="myTab" role="tablist">
                    @foreach($serviceV2 as $k=>$v)
                        <li class="nav-item" role="presentation">
                            <button class="nav-link {{$k == 'basics' ? 'active' : ''}}" id="{{$k}}-tab"
                                    data-bs-toggle="tab" data-bs-target="#{{$k}}"
                                    type="button" role="tab" aria-controls="{{$k}}"
                                    aria-selected="{{$k == 'basics' ? 'true' : 'false'}}">{{ucfirst($k)}}
                            </button>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content" id="myTabContent">
                    @foreach($serviceV2 as $k=>$items)
                        <div class="tab-pane fade {{$k == 'basics' ? 'show active' : ''}}" id="{{$k}}" role="tabpanel" aria-labelledby="{{$k}}-tab">
                            <div class="bg-light p-5">
                                <div class="row justify-content-between">
                                    @foreach($items as $item)
                                        <div class="col-12 col-lg-5 mb-2">
                                            <small class="pull-right">{{$item['price']}}</small>
                                            <p class="lh-1">
                                                <b>{{$item['name']}}</b><br/>
                                                <small class="text-secondary">{!! empty($item['desc']) ? '&nbsp;':$item['desc'] !!}</small>
                                            </p>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    @if(!empty($product))
        <div class="section-margin-top scroll-margin" id="products">
            <div class="container-fluid clearfix bg-light pb-5">
                <div class="container">
                    <h3 class="text-center" style="padding-top: 2.5em">ALPH PRODUCT</h3>

                    <div id="productFlickity">
                        @foreach($product as $item)
                            <div class="popup-gallery carousel-product-cell">
                                <a href="{{asset($item->image_path)}}" data-pswp-width="800" data-pswp-height="800" data-pswp-srcset>
                                    <img src="{{asset($item->image_path)}}" alt="{{$item->name}}" width="100%">
                                </a>
                                <div class="pt-4 pb-0 product-name">{{ucwords(strtolower($item->name))}}</div>
                                <small class="text-smaller text-secondary">{{ucfirst($item->collection)}}</small>
                                <div class="mt-3">RM {{number_format($item->price,2)}}</div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="container clearfix">
        <div class="section-margin-top">
            <div class="career-contents career-contents-desktop">
                <h3>CAREER</h3>
                <p>If you are an outgoing and personable individual looking for an opportunity to work in a fun and friendly salon environment, come join us!</p>
                <p>Fresh graduates are welcome to join our team! If you’re interested in this position talk to us today!</p>
                @if($system_info->fax_number)
                    <div class="text-center w-100">
                        <a class="btn btn-sm btn-outline-dark rounded-0 ps-3 pe-3 mt-3" href="https://api.whatsapp.com/send?phone={{$system_info->fax_number}}" target="_blank">Contact Us</a>
                    </div>
                @endif
            </div>
            <div class="career-imgs">
                <img src="{{asset('images/career.jpg')}}" class="pull-right">
            </div>
            <div class="career-contents career-contents-mobile ">
                <h3>CAREER</h3>
                <p>If you are an outgoing and personable individual looking for an opportunity to work in a fun and friendly salon environment, come join us!</p>
                <p>Fresh graduates are welcome to join our team! If you’re interested in this position talk to us today!</p>
                @if($system_info->fax_number)
                    <div class="text-center w-100">
                        <a class="btn btn-sm btn-outline-dark rounded-0 ps-3 pe-3 mt-3" href="https://api.whatsapp.com/send?phone={{$system_info->fax_number}}" target="_blank">Contact Us</a>
                    </div>
                @endif
            </div>
        </div>
    </div>

    @if(!empty($news))
        <div class="scroll-margin section-margin-top" id="news">
            <div class="container-fluid clearfix">
                <div class="container">
                    <h3 class="text-center">NEWS</h3>

                    <div class="row">
                        @foreach($news->take(4) as $item)
                            <div class="col-12 col-md-6 col-lg-3 mb-4">
                                <div class="news-item">
                                    <img src="{{asset($item->image_path)}}" alt="{{$item->title}}" width="100%">
                                    <div class="mt-3 text-smaller">{{strtoupper($item->news_date->format('M d, Y'))}}</div>
                                    <h4 class="pt-4 pb-0">{{$item->title}}</h4>

                                    <hr class="mt-4 mb-4"/>

                                    <p style="line-height: 1.7em" class="sameHeightNews mb-0 mb-lg-2">
                                        {{\Illuminate\Support\Str::limit($item->content,140)}}
                                    </p>
                                    <a href="{{route('news',['date'=>$item->news_date->toDateString(),'slug'=>$item->slug, 'ib'=>1])}}">
                                        <b class="text-smaller">READ MORE</b>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="section-margin-top">
        <div class="clearfix" style="line-height: 0">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2105.518141996404!2d101.67414209681317!3d3.1256151888879247!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cc4990e088c9b5%3A0xe29d4d272cfdff3!2sNadi+Bangsar!5e0!3m2!1sen!2smy!4v1536904103650"
                    width="100%" height="500px" frameborder="0" style="border:0"></iframe>
        </div>
    </div>

    <div class="clearfix bg-light scroll-margin-less" id="contact">
        <div class="container" style="padding: 5vh 0 10vh;">
            <div class="p-3">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <h3 class="mt-4">CONTACT</h3>
                        <div class="row">
                            @if(!empty($system_info->address))
                                <div class="col-1"><i class="pe-7s-map-marker pe-lg"></i></div>
                                <div class="col-11">
                                    <p class="line-height-1">{!! nl2br($system_info->address) !!}</p>
                                </div>
                            @endif

                            @if(!empty($tels))
                                <div class="col-1"><i class="pe-7s-call pe-lg"></i></div>
                                <div class="col-11">
                                    <p class="line-height-1">
                                        @foreach($tels as $tel)
                                            <a href="tel:{{$tel}}">{{$tel}}</a><br/>
                                        @endforeach
                                    </p>
                                </div>
                            @endif

                            @if(!empty($system_info->email))
                                <div class="col-1"><i class="pe-7s-mail pe-lg"></i></div>
                                <div class="col-11">
                                    <p><a href="mailto:{{$system_info->email}}">{{$system_info->email}}</a></p>
                                </div>
                            @endif

                            <div class="col-1"><i class="pe-7s-clock pe-lg"></i></div>
                            <div class="col-11">
                                <p class="line-height-1 mb-2">Wednesday - Monday<br/>1030 AM - 2000 PM</p>
                                <p class="line-height-1">Tuesday<br/>closed</p>
                            </div>

                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <h3 class="mt-4">GET IN TOUCH</h3>

                        <div class="mb-3">
                            {{ Form::label('name_c', 'Your Name', ['class'=>'form-label']) }}
                            {{ Form::text('name_c', null, ['class'=>'form-control form-control-theme']) }}
                        </div>
                        <div class="mb-3">
                            {{ Form::label('email_c', 'Your Email', ['class'=>'form-label']) }}
                            {{ Form::email('email_c', null, ['class'=>'form-control form-control-theme']) }}
                        </div>
                        <div class="mb-3">
                            {{ Form::label('phone_c', 'Your Phone No.', ['class'=>'form-label']) }}
                            {{ Form::tel('phone_c', null, ['class'=>'form-control form-control-theme']) }}
                        </div>
                        <div class="mb-3">
                            {{ Form::label('message_c', 'Your Message', ['class'=>'form-label']) }}
                            {{ Form::textarea('message_c', null, ['class'=>'form-control form-control-theme','rows'=>3]) }}
                        </div>
                        <div class="mb-3">
                            <button class="btn btn-sm btn-outline-dark rounded-0 ps-3 pe-5" id="contactBtn" onclick="submitContactForm()">
                                Submit
                            </button>
                            <span id="contactFormMsg"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix">
        <div class="w-100" style="background: url({{asset('images/footer-bg.jpg')}}) 50% 0 no-repeat;padding-top: 5vh;background-size: cover;">
            <div class="container">
                <div class="footer-container">
                    <div class="text-center pb-4 mb-5 mt-4">
                        <img src="{{asset('images/logo-white-2023.png')}}" width="160px" alt="{{env('APP_NAME')}}"/>
                    </div>
                    <div class="row">
                        <div class="col-12 col-lg-8 col-md-6 pb-2 mobile-hide">@Copyright {{env('APP_NAME')}} - All rights reserved</div>
                        <div class="col-6 col-lg-2 col-md-3 text-end">
                            <a href="https://www.facebook.com/Alphstudioplus/" target="_blank">
                                <img src="{{asset('images/icon_facebook.png')}}" width="20px" alt="Facebook">
                                &nbsp;&nbsp; {{env('APP_NAME')}}
                            </a>
                        </div>
                        <div class="col-6 col-lg-2 col-md-3 text-end">
                            <a href="https://www.instagram.com/alphstudio/" target="_blank">
                                <img src="{{asset('images/icon_instagram.png')}}" width="20px" alt="Instagram">
                                &nbsp;&nbsp; {{env('APP_NAME')}}
                            </a>
                        </div>

                        <div class="col-12 col-lg-8 col-md-6 pb-2 pt-5 mobile-show-block">@Copyright {{env('APP_NAME')}} - All rights reserved</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if($system_info->fax_number)
        <a style="position: fixed;bottom: 50px;right: 20px;" href="https://api.whatsapp.com/send?phone={{$system_info->fax_number}}" target="_blank"><img src="{{asset('images/whatsapp_icon.png')}}" width="80px"></a>
    @endif
@stop

@section('js')
    <script>
        $.ajaxSetup({
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRF-TOKEN", "{{csrf_token()}}");
                xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            },
            method: 'POST',
            dataType: "json",
            cache: false,
        });

        $(document).ready(function () {
            if ($(window).width() > 576) {

                $('.sameHeightNews').sameHeight();
                $('.sameHeightVision').sameHeight();
            }

            let aboutContainer = $('#about-wrapper');
            aboutContainer.height(aboutContainer.height() - 200);

            $('#masterVisionFlickity').flickity({
                "freeScroll": true,
                "contain": true,
                "autoPlay": true,
                "pageDots": false,
                "prevNextButtons": false,
                "wrapAround": true,
            });
            $('#teamFlickity').flickity({
                "groupCells": true,
                "wrapAround": true,
            });
            $('#artworkFlickity').flickity({
                "cellAlign": "left",
                "contain": true,
                "wrapAround": true,
            });
            $('#productFlickity').flickity({
                "cellAlign": "left",
                "contain": true,
                "prevNextButtons": false,
                "wrapAround": true,
            });
        });

        function submitContactForm() {
            let nameField = $('#name_c');
            let emailField = $('#email_c');
            let phoneField = $('#phone_c');
            let messageField = $('#message_c');
            let contactFormMsg = $('#contactFormMsg');
            let contactBtn = $('#contactBtn');

            contactFormMsg.html('').removeAttr('class');
            nameField.removeClass('is-invalid');
            emailField.removeClass('is-invalid');
            phoneField.removeClass('is-invalid');
            messageField.removeClass('is-invalid');

            let validationFlag = true;
            if (nameField.val() === '') {
                nameField.addClass('is-invalid');
                validationFlag = false;
            }
            if (emailField.val() === '') {
                emailField.addClass('is-invalid');
                validationFlag = false;
            }
            if (phoneField.val() === '') {
                phoneField.addClass('is-invalid');
                validationFlag = false;
            }
            if (messageField.val() === '') {
                messageField.addClass('is-invalid');
                validationFlag = false;
            }

            if (validationFlag === false) {
                contactFormMsg.html('Please fill in all info.').addClass('text-danger');
            } else {
                contactBtn.html('Submitting...').prop("disabled", true);
                $.ajax({
                    url: "{{route('processing')}}",
                    data: JSON.stringify({
                        name: nameField.val(),
                        email: emailField.val(),
                        phone: phoneField.val(),
                        message_c: messageField.val(),
                        type: 'contact'
                    }),
                    success: function (response) {
                        if ('err_msg' in response) {
                            contactFormMsg.html(response.err_msg).addClass('text-danger');
                        } else {
                            nameField.val('');
                            emailField.val('');
                            phoneField.val('');
                            messageField.val('');
                            contactFormMsg.html('Submitted! We will get back to you soonest.').addClass('text-success');
                        }
                        contactBtn.html('Submit').prop("disabled", false);
                    },
                    error: function (e) {
                        contactFormMsg.html(e).addClass('text-danger');
                        contactBtn.html('Submit').prop("disabled", false);
                    }
                });
            }
        }

        var lightbox = new PhotoSwipeLightbox({
            gallery: '.popup-gallery',
            children: 'a',
            pswpModule: PhotoSwipe
        });
        lightbox.init();
    </script>
@stop
